package cn.alcohol.heavenly.palace.client.bootstrap;

import cn.alcohol.heavenly.palace.client.initializer.NettyClientInitializer;
import cn.alcohol.heavenly.palace.common.bootstrap.BootstrapLifeCycle;
import cn.alcohol.heavenly.palace.common.constants.CommonConstant;
import cn.alcohol.heavenly.palace.common.exception.LifeCycleException;
import cn.alcohol.heavenly.palace.common.tool.CommonTool;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollMode;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetSocketAddress;

@Slf4j
@Component
public class HeavenlyPalaceClient implements BootstrapLifeCycle {

    private EventLoopGroup eventLoopGroup;
    private Bootstrap bootstrap;
    private Channel channel;

    @Resource
    private NettyClientInitializer nettyClientInitializer;

    @Override
    public void startup() throws LifeCycleException {
        bootstrap = this.init();
        
        this.connect("21312", 123123);
    }

    @Override
    public void shutdown() throws LifeCycleException {
        eventLoopGroup.shutdownGracefully();

        if (isConnected()) {
            try {
                channel.close().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Bootstrap init() {
        boolean isEpoll = CommonTool.isEpoll();

        this.eventLoopGroup = isEpoll ? new EpollEventLoopGroup(10 * CommonConstant.DEFAULT_CORE_THREAD) : new NioEventLoopGroup(10 * CommonConstant.DEFAULT_CORE_THREAD);

        Bootstrap bootstrap = new Bootstrap()
                .group(eventLoopGroup)
                .channel(isEpoll ? EpollSocketChannel.class : NioSocketChannel.class)
                .handler(nettyClientInitializer)
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

        if (isEpoll) {
            bootstrap.option(EpollChannelOption.EPOLL_MODE, EpollMode.LEVEL_TRIGGERED)
                    .option(EpollChannelOption.TCP_FASTOPEN_CONNECT, true);
        }

        return bootstrap;
    }

    private boolean isConnected() {
        return this.channel != null && this.channel.isOpen() && this.channel.isActive();
    }

    public void send(Object object) {
        send(object, false);
    }

    public void send(Object object, boolean sync) {
        if (isConnected()) {
            if (sync) {
                try {
                    channel.writeAndFlush(object).sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                if(channel.eventLoop() == null || channel.eventLoop().inEventLoop()) {
                    channel.writeAndFlush(object);
                } else {
                    channel.eventLoop().execute(() -> channel.writeAndFlush(object));
                }
            }
        }
    }

    public void close() {
//        getEventHandler().unregisterAll();
        group.shutdownGracefully();
        closeChannel();
    }

    public void connect(String host, int port) {
        // Close the Channel if it's already connected
        if (!isConnected()) {
            closeChannel();
        }
        // Start the client and wait for the connection to be established.
        try {
            this.channel = this.bootstrap.connect(new InetSocketAddress(host, port)).sync().channel();
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
    }
}
