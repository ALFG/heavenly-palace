package cn.alcohol.heavenly.palace.client.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix ="server.netty")
public class ClientNettyProperties {

    private int port;
    private int thread;
    private int backlog;
    private boolean reuseaddr;
    private int revbuf;
    private boolean tcpNodelay;
    private boolean keepalive;
}
