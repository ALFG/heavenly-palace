package cn.alcohol.heavenly.palace.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeavenlyPalacePrivoderApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeavenlyPalacePrivoderApplication.class, args);
    }

}
