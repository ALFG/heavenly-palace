package cn.alcohol.heavenly.palace.common.exception;

public class LifeCycleException extends RuntimeException {

    public LifeCycleException(String message) {
        super(message);
    }
}
