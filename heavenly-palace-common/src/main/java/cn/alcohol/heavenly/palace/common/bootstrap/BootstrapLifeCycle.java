package cn.alcohol.heavenly.palace.common.bootstrap;


import cn.alcohol.heavenly.palace.common.exception.LifeCycleException;

public interface BootstrapLifeCycle {

    void startup() throws LifeCycleException;

    void shutdown() throws LifeCycleException;
}
