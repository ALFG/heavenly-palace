package cn.alcohol.heavenly.palace.common.codec;

import cn.alcohol.heavenly.palace.common.transport.KryoSerialization;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class KryoNettyEncoder extends MessageToByteEncoder<Object> {

	private final int inputSize;
	private final int outputSize;
	private final int maxOutputSize;

	public KryoNettyEncoder(int inputSize, int outputSize, int maxOutputSize) {
		this.inputSize = inputSize;
		this.outputSize = outputSize;
		this.maxOutputSize = maxOutputSize;
	}


	protected void encode (ChannelHandlerContext ctx, Object object, ByteBuf out) {
		byte[] objectBytes = new KryoSerialization(inputSize, outputSize, maxOutputSize).encode(object);
		out.writeInt(objectBytes.length);
		out.writeBytes(objectBytes);
	}
}
