package cn.alcohol.heavenly.palace.common.tool;

import cn.alcohol.heavenly.palace.common.constants.CommonConstant;
import io.netty.channel.epoll.Epoll;
import org.apache.commons.lang3.StringUtils;

public interface CommonTool {

    static boolean isEpoll() {
        return StringUtils.isEmpty(CommonConstant.OS_NAME) &&
                StringUtils.contains(CommonConstant.OS_NAME, "linux") &&
                Epoll.isAvailable();
    }
}
