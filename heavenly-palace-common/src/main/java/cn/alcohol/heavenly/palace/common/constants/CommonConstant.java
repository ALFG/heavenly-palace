package cn.alcohol.heavenly.palace.common.constants;

public interface CommonConstant {

    String OS_NAME = System.getProperty("os.name").toLowerCase();
    String BOSS_EVENT_GROUP_NAME_PREFIX = "netty-server-boss";
    String WORK_EVENT_GROUP_NAME_PREFIX = "netty-server-work";

    int DEFAULT_CORE_THREAD =  Runtime.getRuntime().availableProcessors();
}
