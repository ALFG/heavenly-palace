package cn.alcohol.heavenly.palace.common.transport;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializerFactory;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.JavaSerializer;
import com.esotericsoftware.kryo.util.Pool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

public class KryoSerialization {

    private final Pool<Kryo> kryoPool;
    private final Pool<Input> inputPool;
    private final Pool<Output> outputPool;

    public KryoSerialization(int inputSize, int outputSize, int maxOutputSize) {
        kryoPool = new Pool<>(true, true) {
            @Override
            protected Kryo create() {
                SerializerFactory.CompatibleFieldSerializerFactory factory = new SerializerFactory.CompatibleFieldSerializerFactory();
                factory.getConfig().setFieldsCanBeNull(true);
                factory.getConfig().setFieldsAsAccessible(true);
                factory.getConfig().setIgnoreSyntheticFields(true);
                factory.getConfig().setFixedFieldTypes(false);
                factory.getConfig().setCopyTransient(true);
                factory.getConfig().setSerializeTransient(false);
                factory.getConfig().setVariableLengthEncoding(true);
                factory.getConfig().setExtendedFieldNames(true);
                factory.getConfig().setReadUnknownFieldData(false);

                Kryo kryo = new Kryo();
                kryo.setRegistrationRequired(true);
                kryo.setReferences(true);
                kryo.addDefaultSerializer(Throwable.class, new JavaSerializer());
                kryo.setDefaultSerializer(factory);

                kryo.register(HashMap.class, 10);
                kryo.register(ArrayList.class, 11);
                kryo.register(HashSet.class, 12);
                kryo.register(byte[].class, 13);
                kryo.register(char[].class, 14);
                kryo.register(short[].class, 15);
                kryo.register(int[].class, 16);
                kryo.register(long[].class, 17);
                kryo.register(float[].class, 18);
                kryo.register(double[].class, 19);
                kryo.register(boolean[].class, 20);
                kryo.register(String[].class, 21);
                kryo.register(Object[].class, 22);
                kryo.register(BigInteger.class, 23);
                kryo.register(BigDecimal.class, 24);
                kryo.register(Class.class, 25);
                kryo.register(Date.class, 26);
                kryo.register(StringBuffer.class, 27);
                kryo.register(StringBuilder.class, 28);
                kryo.register(Collections.EMPTY_LIST.getClass(), 29);
                kryo.register(Collections.EMPTY_MAP.getClass(), 30);
                kryo.register(Collections.EMPTY_SET.getClass(), 31);
                kryo.register(Collections.singleton(null).getClass(), 32);
                kryo.register(Collections.singletonList(null).getClass(), 33);
                kryo.register(Collections.singletonMap(null, null).getClass(), 34);
                kryo.register(TreeSet.class, 35);
                kryo.register(Collection.class, 36);
                kryo.register(TreeMap.class, 37);
                kryo.register(Map.class, 38);
                kryo.register(TimeZone.class, 39);
                kryo.register(Calendar.class, 40);
                kryo.register(Locale.class, 41);
                kryo.register(Charset.class, 42);
                kryo.register(URL.class, 43);
                kryo.register(Collections.emptyList().getClass(), 44);
                kryo.register(PriorityQueue.class, 45);
                kryo.register(BitSet.class, 46);

                // Register KryoNetty Classes
//                if (!kryoNetty.getClassesToRegister().isEmpty())
//                    kryoNetty.getClassesToRegister().forEach((key, value) -> kryo.register(value, (key + 100)));

                return kryo;
            }
        };

        inputPool = new Pool<>(true, true) {
            @Override
            protected Input create() {
                return new Input(inputSize == 0 ? 2048 : inputSize);
            }
        };

        outputPool = new Pool<>(true, true) {
            @Override
            protected Output create() {
                return new Output(outputSize == 0 ? 2048 : outputSize, maxOutputSize == 0 ? -1 : maxOutputSize);
            }
        };
    }

    public Kryo obtainKryo() {
        return kryoPool.obtain();
    }

    public Input obtainInput() {
        return inputPool.obtain();
    }

    public Output obtainOutput() {
        return outputPool.obtain();
    }

    public void free(Kryo kryo) {
        kryoPool.free(kryo);
    }

    public void free(Input input) {
        inputPool.free(input);
    }

    public void free(Output output) {
        outputPool.free(output);
    }


    public <T> byte[] encode(T object) {
        Kryo kryo = obtainKryo();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Output output = obtainOutput();
        output.setOutputStream(outputStream);
        kryo.writeClassAndObject(output, object);
        output.flush();
        output.close();
        free(kryo);
        free(output);
        return outputStream.toByteArray();
    }

    public <T> T decode(byte[] bytes) {
        Kryo kryo = obtainKryo();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        Input input = obtainInput();
        input.setInputStream(inputStream);
        T object = (T) kryo.readClassAndObject(input);
        input.close();
        free(kryo);
        free(input);
        return object;
    }
}
