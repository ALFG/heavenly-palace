package cn.alcohol.heavenly.palace.common.codec;

import cn.alcohol.heavenly.palace.common.transport.KryoSerialization;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class KryoNettyDecoder extends ByteToMessageDecoder {

	private final int inputSize;
	private final int outputSize;
	private final int maxOutputSize;

	public KryoNettyDecoder(int inputSize, int outputSize, int maxOutputSize) {
		this.inputSize = inputSize;
		this.outputSize = outputSize;
		this.maxOutputSize = maxOutputSize;
	}

	@Override
	protected void decode (ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
		if (in.readableBytes() < 2) {
			return;
		}

		in.markReaderIndex();

		int contentLength = in.readInt();
		if (in.readableBytes() < contentLength) {
			in.resetReaderIndex();
			return;
		}

		byte[] objectBytes = new byte[contentLength];
		in.readBytes(objectBytes);

		Object object = new KryoSerialization(inputSize, outputSize, maxOutputSize).decode(objectBytes);
		out.add(object);
	}
}
