package cn.alcohol.heavenly.palace.server.bootstrap;

import cn.alcohol.heavenly.palace.common.bootstrap.BootstrapLifeCycle;
import cn.alcohol.heavenly.palace.common.bootstrap.NamedThreadFactory;
import cn.alcohol.heavenly.palace.common.constants.CommonConstant;
import cn.alcohol.heavenly.palace.common.tool.CommonTool;
import cn.alcohol.heavenly.palace.common.tool.IpTool;
import cn.alcohol.heavenly.palace.server.initializer.NettyServerInitializer;
import cn.alcohol.heavenly.palace.server.properties.ServerNettyProperties;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollMode;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Component
public class HeavenlyPalaceServer implements BootstrapLifeCycle {

    @Resource
    private NettyServerInitializer nettyServerInitializer;
    @Resource
    private ServerNettyProperties properties;

    private final AtomicBoolean isStarted = new AtomicBoolean(false);

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private ServerBootstrap bootstrap;

    @Override
    public void startup() {
        if (isStarted.compareAndSet(true, false)) {
            return;
        }

        this.init();

        bootstrap.bind(properties.getPort()).addListener((ChannelFutureListener) channelFuture -> {
            if (channelFuture.isSuccess())
                log.info("Heavenly Palace Server 启动成功【" + IpTool.host() + ":" + properties.getPort() + "】");
            else
                log.info("Heavenly Palace Server 启动失败【" + IpTool.host() + ":" + properties.getPort() + "】");
        });
    }

    @Override
    public void shutdown() {
        if (isStarted.compareAndSet(false, true)) {
            return;
        }

        if (workerGroup != null && bossGroup != null) {
            try {
                bossGroup.shutdownGracefully().sync();
                workerGroup.shutdownGracefully().sync();
            } catch (InterruptedException e) {
                log.info("Heavenly Palace Server 关闭资源失败【" + IpTool.host() + ":" + properties.getPort() + "】");
            }
        }
    }

    private void init() {
        boolean isEpoll = CommonTool.isEpoll();

        int bossThread = properties.getBossThread();
        bossThread = (bossThread == 0) ? CommonConstant.DEFAULT_CORE_THREAD * 2 : bossThread;

        int workThread = properties.getBossThread();
        workThread = (workThread == 0) ? CommonConstant.DEFAULT_CORE_THREAD * 10 : workThread;

        bootstrap = new ServerBootstrap();
        if (isEpoll) {
            bossGroup = new EpollEventLoopGroup(bossThread, new NamedThreadFactory(CommonConstant.BOSS_EVENT_GROUP_NAME_PREFIX, false));
            workerGroup = new EpollEventLoopGroup(workThread, new NamedThreadFactory(CommonConstant.WORK_EVENT_GROUP_NAME_PREFIX, true));
        } else {
            bossGroup = new NioEventLoopGroup(bossThread, new NamedThreadFactory(CommonConstant.BOSS_EVENT_GROUP_NAME_PREFIX, false));
            workerGroup = new NioEventLoopGroup(workThread, new NamedThreadFactory(CommonConstant.WORK_EVENT_GROUP_NAME_PREFIX, true));
        }

        bootstrap.group(bossGroup, workerGroup)
                .channel(isEpoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
                .childHandler(nettyServerInitializer)
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.IP_TOS, 24)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.SO_REUSEADDR, true);

        if(isEpoll) {
            bootstrap
                    .childOption(EpollChannelOption.EPOLL_MODE, EpollMode.LEVEL_TRIGGERED)
                    .option(EpollChannelOption.TCP_FASTOPEN, 3)
                    .option(EpollChannelOption.SO_REUSEPORT, true);
        }
    }
}
