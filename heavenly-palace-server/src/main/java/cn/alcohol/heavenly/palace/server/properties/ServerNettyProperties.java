package cn.alcohol.heavenly.palace.server.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix ="server.netty")
public class ServerNettyProperties {

    private int port;
    private int bossThread;
    private int workThread;
    private int backlog;
    private boolean reuseaddr;
    private int revbuf;
    private boolean tcpNodelay;
    private boolean keepalive;
}
