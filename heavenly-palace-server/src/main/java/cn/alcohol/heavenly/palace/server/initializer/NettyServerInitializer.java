package cn.alcohol.heavenly.palace.server.initializer;

import cn.alcohol.heavenly.palace.common.codec.KryoNettyDecoder;
import cn.alcohol.heavenly.palace.common.codec.KryoNettyEncoder;
import cn.alcohol.heavenly.palace.server.handler.NettyServerHandler;
import cn.alcohol.heavenly.palace.server.properties.KryoProperties;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class NettyServerInitializer extends ChannelInitializer<SocketChannel> {

    @Resource
    private NettyServerHandler nettyServerHandler;
    @Resource
    private KryoProperties properties;

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        int inputSize = properties.getInputSize();
        int outputSize = properties.getOutputSize();
        int maxOutputSize = properties.getMaxOutputSize();

        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast("logging", new LoggingHandler(LogLevel.INFO));
        pipeline.addLast("decoder", new KryoNettyDecoder(inputSize, outputSize, maxOutputSize));
        pipeline.addLast("encoder", new KryoNettyEncoder(inputSize, outputSize, maxOutputSize));
        pipeline.addLast("nettyServerHandler", nettyServerHandler);
    }
}
