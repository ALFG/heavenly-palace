package cn.alcohol.heavenly.palace.server;

import cn.alcohol.heavenly.palace.server.bootstrap.HeavenlyPalaceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

@SpringBootApplication
public class HeavenlyPalaceServerApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(HeavenlyPalaceServerApplication.class);

        springApplication.addListeners((ApplicationListener<ApplicationReadyEvent>) event -> {
            HeavenlyPalaceServer heavenlyPalaceServer = (HeavenlyPalaceServer) event.getApplicationContext().getBean("heavenlyPalaceServer");
            heavenlyPalaceServer.startup();
        });
        springApplication.addListeners((ApplicationListener<ApplicationFailedEvent>) event -> {
            HeavenlyPalaceServer heavenlyPalaceServer = (HeavenlyPalaceServer) event.getApplicationContext().getBean("heavenlyPalaceServer");
            heavenlyPalaceServer.shutdown();
        });

        springApplication.run(args);
    }
}
