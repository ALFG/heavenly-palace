package cn.alcohol.heavenly.palace.server.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix ="kryo")
public class KryoProperties {

    private int inputSize;
    private int outputSize;
    private int maxOutputSize;
}
